import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {CookieService} from 'ngx-cookie-service';

import {RestangularModule, Restangular} from 'ngx-restangular';

import {RestangularAuth} from './app.service';
import {OrdersComponent} from './orders/orders.component';
import {SigninComponent} from './signin/signin.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {Headers, Http} from '@angular/http';

import TokenRefreshService from './token-refresh.service';

// Function for setting the default restangular configuration
export function RestangularConfigFactory(RestangularProvider, http: Http, cookieService: CookieService, 
  tokenRefreshService: TokenRefreshService) {
  RestangularProvider.setBaseUrl('https://api.wispapp.com');
  RestangularProvider.addFullRequestInterceptor((element, operation, path, url, headers) => {

    let token = '';

    if (cookieService.check('authData')) {
      token = JSON.parse(cookieService.get('authData'))['access_token'];
    }


    return {
      headers: Object.assign({}, headers, { Authorization: `Bearer ${token}` })
    };
  });

   RestangularProvider.addErrorInterceptor((response, subject, responseHandler) => {
    if (response.status === 403 || response.status === 401) {

      tokenRefreshService.refreshToken()
        .switchMap(refreshAccesstokenResponse => {
          console.log('switchMap');
          console.log(refreshAccesstokenResponse);

          response.request.headers.set('Authorization', 'Bearer ' + refreshAccesstokenResponse);

          return response.repeatRequest(response.request);
        })
        .subscribe(
        res => responseHandler(res),
        err => subject.error(err)
        );

      return false;
    }
    return true;
  });
}

// Function for setting the auth restangular configuration
export function RestangularAuthFactory(restangular: Restangular, http: Http, cookieService: CookieService) {
  return restangular.withConfig((RestangularConfigurer) => {
    RestangularConfigurer.setBaseUrl('https://api.wispapp.com');

    RestangularConfigurer.addFullRequestInterceptor((element, operation, path, url, headers) => {

      let token = ''; // when we have multiple api-backend services

      if (cookieService.check('authData')) {
        token = JSON.parse(cookieService.get('authData'))['access_token'];
      }

      return {
        headers: Object.assign({}, headers, { Authorization: `Bearer ${token}` })
      };
    });
  });
}


@NgModule({
  declarations: [
    AppComponent,
    OrdersComponent,
    SigninComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, ReactiveFormsModule,
    RestangularModule.forRoot([Http, CookieService, TokenRefreshService], RestangularConfigFactory)
  ],
  providers: [
    CookieService,
    {
      provide: RestangularAuth,
      useFactory: RestangularAuthFactory,
      deps: [Restangular, Http, CookieService]
    }, TokenRefreshService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
