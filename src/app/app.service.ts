import {InjectionToken} from '@angular/core';

export const RestangularAuth = new InjectionToken<any>('RestangularAuth');

/* RestangularAuth - for example for auth in the feature */

export class AppService {

  constructor() {
  }

}
