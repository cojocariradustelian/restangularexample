import {Injectable, Inject} from '@angular/core';
import {Restangular} from 'ngx-restangular';
import {RestangularAuth} from './app.service';

@Injectable()
export class AuthApiService {

  constructor(@Inject(Restangular) public restangular, @Inject(RestangularAuth) private restangularAuth) {
  }

  getToken(data) {
    const customData = data;

    customData['grant_type'] = 'password';
    customData['client_id'] = 'admin-panel';

    const str = [];
    for (const key in customData) {
      if (customData.hasOwnProperty(key)) {
        str.push(encodeURIComponent(key) + '=' + encodeURIComponent(customData[key]));
      }
    }
    const resultStr = str.join('&').toString();  // encode for www-urlencode method

    return this.restangularAuth.all(`token`).customPOST(
      resultStr,
      undefined,
      undefined,
      {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
    );
  }

  getNewToken(data) {
    const customData = data;

    customData['grant_type'] = 'refresh_token';
    customData['client_id'] = 'admin-panel';

    const str = [];
    for (const key in customData) {
      if (customData.hasOwnProperty(key)) {
        str.push(encodeURIComponent(key) + '=' + encodeURIComponent(customData[key]));
      }
    }
    const resultStr = str.join('&').toString();  // encode for www-urlencode method

    return this.restangularAuth.all(`token`).customPOST(
      resultStr,
      undefined,
      undefined,
      {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
    );
  }

}
