import {Component, OnInit} from '@angular/core';
import {OrderApiService} from "../order-api.service";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
  providers: [OrderApiService]
})
export class OrdersComponent implements OnInit {
  appId = '5968b408d57f0f23c0494f15';

  constructor(private orderService: OrderApiService) {
  }

  ngOnInit() {
  }

  getOrdersQuery(id) {
    this.orderService.getTypes(id).subscribe((res) => {
      console.log('success response ' + id + ': ', res);
    }, (err) => {
      console.log('error response ' + id + ': ', err);
    })
  }

  getValidOrders() {
    this.getOrdersQuery(this.appId);
    this.getOrdersQuery(this.appId);
    this.getOrdersQuery(this.appId);
  }

  getInvalidOrders() {
    this.getOrdersQuery(this.appId + '_invalid');
    this.getOrdersQuery(this.appId + '_invalid');
    this.getOrdersQuery(this.appId + '_invalid');
  }

}
