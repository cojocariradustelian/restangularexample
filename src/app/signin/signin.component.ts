import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AuthApiService} from '../auth-api.service';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
  providers: [AuthApiService]
})
export class SigninComponent implements OnInit {

  signInForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private authService: AuthApiService,
              private cookieService: CookieService) {
    this.signInForm = this.formBuilder.group({
      username: [],
      password: []
    });
  }

  ngOnInit() {
  }

  signIn() {
    console.log(this.signInForm.value);

    this.authService.getToken(this.signInForm.value).subscribe((res) => {
      console.log('success response getToken: ', res);

      this.cookieService.set('authData', JSON.stringify(res.plain()));
    }, (err) => {
      console.log('error response getToken: ', err);
    });
  }

}
