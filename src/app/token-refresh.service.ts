import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export default class TokenRefreshService {
    source: Observable<any>;
    pausedObservable: Observable<any>;
    constructor(
        private http: Http,
        private cookieService: CookieService) {
        this.source = Observable.defer(() => {
            return this.postRequest();
        }).share();
    }

    refreshToken(): Observable<any> {
        return this.source
            .map((data) => data.json().access_token);
    }

    private postRequest(): Observable<any> {
        console.log('Getting token');
        const cookieData = JSON.parse(this.cookieService.get('authData'));
        const customData = {};

        customData['grant_type'] = 'refresh_token';
        customData['client_id'] = 'admin-panel';
        customData['refresh_token'] = cookieData['refresh_token'];

        const header = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });

        const str = [];
        for (const key in customData) {
            if (customData.hasOwnProperty(key)) {
                str.push(encodeURIComponent(key) + '=' + encodeURIComponent(customData[key]));
            }
        }
        const resultStr = str.join('&').toString();  // encode for www-urlencode method


        return this.http.post(`https://api.wispapp.com/token`, resultStr, { headers: header });
    }
}